# cert-hash-check

Checking the hash of a certificate, the coexisting files and check if you're able to create standardised symbolic links to the original certificate files. And maybe more!

## How to setup:

* Add the folder `$HOME/cert-hash-check` to your PATH, typing `export PATH=$PATH:~/cert-hash-check`
* If you need it permanently, add that last line in your `~/.bashrc`. If you're using zsh, then add it to `~/.zshrc` instead.

```bash
export PATH="$PATH:$HOME/cert-hash-check"
```

## How to use:

Usage: 

```bash
cert_hash_check [DOMAIN] [YYYY] [PERIOD] [CERT_PATH]
```

Example: 
```bash
cert_hash_check domain.tld 2020 1y /etc/pki/tls/certs/
```


